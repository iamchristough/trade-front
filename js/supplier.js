$(function () {

    var nav = $('.dropDownMenu');
    var dropDownMenu = $('.drop_downUL');
    var dropDownMenu_li = dropDownMenu.find('li');
    var filterList = $('.filterResults');
    var dropDown_typeOfService = 0;
    var supplierListing = $('.listing');
    var fuzzy_search_results;
    var checkbox_array = [];
    var count = 0;
    var selected_typeOfService = 0;
    var noResults_error = $('.noResults');
    var overseas_typeOfService_dropDown = $('.overseas-service-list');
    var uk_typeOfService_dropDown = $('.uk-service-list');
    var search_box = $('.searchBox');
    var rb_all = $('.all');
    var rb_overseas = $('.overseas');
    var rb_uk = $('.ukTrade');

    overseas_typeOfService_dropDown.hide();
    uk_typeOfService_dropDown.hide();
    rb_all.addClass('selected');


    /**
     * Closes the listings.
     */
    var close_listings = function() {
        supplierListing.each(function (){
            if($(this).hasClass('open')) {
                $(this).toggleClass('open');
            }
        })
    }

    /**
     * Clears the checkboxes.
     */
    var clear_checkboxes = function() {
        $('.filterListBox').each(function (){
            if($(this).hasClass('.active')){
                $(this).find('label').trigger('click');
            }
        })
    }

    /**
     * Opens and closes the drop down menu.
     *
     * @param {evt} the drop down menu which is being clicked.
     */
    $("h2.drop_down").click(function (evt) {
        if (dropDownMenu.hasClass('active')) {

            dropDownMenu.removeClass("active");
        }
        else {

            evt.stopPropagation();
            var $ul = $(this).siblings(".drop_downUL");

            dropDownMenu.removeClass("active");
            $ul.addClass("active");

        }

    });

    /**
     * Changes the text of the menu to the menu item that gets selected.
     */

    dropDownMenu.on('click', 'li', function () {

        dropDownMenu_li.removeClass('active');
        $(this).addClass('active');
        clear_checkboxes();
        close_listings();
        var selectedValue = $(this).text();

        $('.selectedServiceInput').val(selectedValue);
        var newItem = $('.selectedServiceInput').val();
        $(this).parent().parent().find('.inner-text').text(newItem);
        nav.trigger('click');

    });

    /**
     * Checks if the listing has a location of uk or overseas &
     * shows the listing if the supplier location is the same as the listing
     */
    var selected_supplier_location = function (listing_supplierLocation, listItem) {

        if (listing_supplierLocation === "uk" && rb_uk.hasClass('selected')) { //if listing is UK and UK is selected
            listItem.show();
        }
        else if (listing_supplierLocation === "overseas" && rb_overseas.hasClass('selected')) { //if listing is overseas and overseas is selected
            listItem.show();
        }
        else {
            if (rb_all.hasClass('selected')) { //if supplier location 'all' is selected
                listItem.show();
            }
        }

    }


    /**
     * Shows the correct listings depending on the current region AND/OR type of service
     * @param - current selected region type, current type of service selected
     */

    var dataAttrValidation = function (selected_region, selected_typeOfService) {
        // goes through each list
        var hideListing = 0;
        supplierListing.each(function () {
            // store the attributes 'data-type' for each listing
            var listing_typeOfService = $(this).attr('data-type');
            // store the attributes 'data-region-option' for each listing
            var listing_regions = $(this).attr('data-region-option').split(",");
            var listing_supplierLocation = $(this).attr('data-supplier-location');
            var listItem = $(this);

            // if the selected region has a 'data-type' then

            // If Region dropdown is not empty
            if (selected_region !== undefined) {
                // Loop through each list and loop through data
                // if the 'data-type' of the listing is equal to the 'data-type' of the drop_down-service list item
                if (selected_typeOfService === listing_typeOfService || selected_typeOfService === undefined  || selected_typeOfService === '') {
                    // iterate through the regions of the listing
                    for (var i = 0; i < listing_regions.length; i++) {
                        // if region of the listing is equal to the selected region then show the listing
                        if (selected_region === listing_regions[i]) {

                            selected_supplier_location(listing_supplierLocation, listItem)

                        }
                    }
                }

                if ($(this).is(':visible')) {
                    noResults_error.hide();
                }
                else {
                    hideListing++;
                }

                if (hideListing === supplierListing.length) {
                    noResults_error.show();
                }
            }
            // if no region is selected then
            else if (selected_typeOfService === listing_typeOfService && selected_typeOfService !== undefined || selected_typeOfService === '') { // if the 'data-type' of the listing is equal to the 'data-type' of the drop_down-service list item

                selected_supplier_location(listing_supplierLocation, listItem)
            }
            else {

                selected_supplier_location(listing_supplierLocation, listItem)
            }

        })
    }

    /**
     * Function for drop_down-service drop downs
     */
    var drop_down_services = function (selected_li_data_id, selected_li_data_type, selected_region) {

        var hideListing = 0;
        dropDown_typeOfService = selected_li_data_type;

        // hides checkbox sections
        $("[data-section]").hide();

        // hides all the listings
        supplierListing.hide();

        // shows checkbox sections
        for (var i = 0; i < selected_li_data_id.length; i++) {
            $("[data-section='section_" + selected_li_data_id[i] + "']").show();
        }

        // shows listings
        supplierListing.each(function () {
            //Split Data Attributes in each list item
            var listing_typeOfService = $(this).attr('data-type');
            var listing_regions = $(this).attr('data-region-option').split(",");
            var listing_supplierLocation = $(this).attr('data-supplier-location');
            var listItem = $(this);

            // If Region dropdown is not empty
            if (selected_region !== undefined) {
                // Loop through each list and loop through data
                // if the 'data-type' of the listing is equal to the 'data-type' of the drop_down-service list item
                if (dropDown_typeOfService === listing_typeOfService) {
                    // iterate through the regions of the listing
                    for (var i = 0; i < listing_regions.length; i++) {
                        // if region of the listing is equal to the selected region then show the listing
                        if (selected_region === listing_regions[i]) {

                            selected_supplier_location(listing_supplierLocation, listItem)

                        }
                    }
                }

            }
            // if no region is selected then
            else {

                // if the 'data-type' of the listing is equal to the 'data-type' of the drop_down-service list item
                if (dropDown_typeOfService === listing_typeOfService) {

                    selected_supplier_location(listing_supplierLocation, listItem)
                }

            }

            if ($(this).is(':visible')) {
                noResults_error.hide();
            }
            else {
                hideListing++;
            }

            if (hideListing === supplierListing.length) {
                noResults_error.show();
            }
        });

    }
    /**
     * Hides the listings and sets the inner-text to have the same attribute as the selected list item.
     * Passes in the dataAttrValidation function
     */
    $('.drop_down-region').on('click', 'li', function () {
        noResults_error.hide();
        // hide all the listings
        supplierListing.hide();
        // stores the 'data-region' attribute of the selected list item
        var selectedRegionType = $(this).attr("data-region");
        // set the attribute of the inner text to the same 'data-region' as the selected list item
        $('.region-list').find('.inner-text').attr("data-region", selectedRegionType);
        selected_typeOfService = $('.hidden_user_selection').attr("data-type");

        // empty the search box
        search_box.val('');

        if (rb_uk.hasClass('selected') || rb_overseas.hasClass('selected')) {
            dataAttrValidation(selectedRegionType, selected_typeOfService);
        }
        else {
            var hideListing = 0;
            supplierListing.each(function () {

                var selected_region = $('.region-list').find('.inner-text').attr('data-region');
                var listingRegion = $(this).attr('data-region-option').split(',');

                if (selected_region === undefined) {
                    supplierListing.show()
                }
                else {
                    for (i = 0; i < listingRegion.length; i++) {
                        if (selected_region === listingRegion[i]) {
                            $(this).show()
                        }
                    }

                    if ($(this).is(':visible')) {
                        noResults_error.hide();
                    }
                    else {
                        hideListing++;
                    }
                }

                if (hideListing === supplierListing.length) {
                    noResults_error.show();
                }
            });
        }
    });

    /**
     * Closes the drop down menu if the user clicks anywhere outside of it.
     */
    $('html').click(function () {
        dropDownMenu.removeClass('active');
    });


    /**
     * When the user selects an item from 'Type of Service Dropdown'. it will show the filter section.
     */
    $('.drop_down-service-uk').on('click', 'li', function () {

        noResults_error.hide();
        filterList.addClass('active');
        var selected_li_data_id = $(this).attr("data-id").split(",");
        var selected_li_data_type = $(this).attr("data-type");
        var selected_region = $('.region-list').find('.inner-text').attr('data-region');
        // gives inner-text of uk-service-list the same 'data-type' as the clicked list item
        uk_typeOfService_dropDown.find('.inner-text').attr("data-type", selected_li_data_type);
        uk_typeOfService_dropDown.find('.inner-text').text(selected_li_data_type);

        $('.hidden_user_selection').attr("data-type", selected_li_data_type);

        // empty the search box
        search_box.val('');

        drop_down_services(selected_li_data_id, selected_li_data_type, selected_region);

    });


    /**
     * When the user selects an item from 'Type of Service Dropdown'. it will show the filter section.
     */
    $('.drop_down-service-overseas').on('click', 'li', function () {

        console.log('clicked');

        noResults_error.hide();
        filterList.addClass('active');
        var selected_li_data_id = $(this).attr("data-id").split(",");
        var selected_li_data_type = $(this).attr("data-type");
        var selected_region = $('.region-list').find('.inner-text').attr('data-region');
        // gives inner-text of overseas-service-list the same 'data-type' as the clicked list item
        overseas_typeOfService_dropDown.find('.inner-text').attr("data-type", selected_li_data_type);
        overseas_typeOfService_dropDown.find('.inner-text').text(selected_li_data_type);
        $('.hidden_user_selection').attr("data-type", selected_li_data_type);

        // empty the search box
        search_box.val('');


        drop_down_services(selected_li_data_id, selected_li_data_type, selected_region);

    });

    /*-------------------------
     // Beginning of Fuzzy Search Section
     ----------------------------*/
    var searchArr = [];

    supplierListing.each(function (i) {
        var $infoHeader = $(this).find('.infoHeader').text();
        var $id = $(this).attr("data-id");
        var $type = $(this).attr("data-type");
        var temp = {
            id: $id,
            type: $type,
            title: $infoHeader
        };
        searchArr.push(temp);
    });

    var options = {
        keys: ['title']
    }

    /*-------------------------
     // End of Fuzzy Search Section
     ----------------------------*/

    /*-------------------------
     // Iterate through checkbox function
     ----------------------------*/
    var checkbox_iterate = function (listId, show, listing_typeOfService, listItem, listing_supplierLocation) {

        // iterate over the checkbox_array
        for (var i = 0; i < checkbox_array.length; i++) {
            if (listId.indexOf(checkbox_array[i]) !== -1) { // if 'data-id-option' exists
                show = true; // set to true
                break; // break the loop
            }

        }
        // if show is true AND the 'data-type' of the listing is the same as the selected 'type of service'
        if (show && listing_typeOfService === dropDown_typeOfService) {
            selected_supplier_location(listing_supplierLocation, listItem)
        }

    }



    /*-------------------------
     // Search Input - Keyboard Press
     ----------------------------*/

    search_box.on('change paste keyup', function () {

        // fuzzy search
        var f = new Fuse(searchArr, options);
        fuzzy_search_results = f.search(search_box.val());
        // Sets selected_region to the value of inner-texts 'data-region' attribute
        var selected_region = $('.region-list').find('.inner-text').attr('data-region');
        // hide all listings
        supplierListing.hide();

        var selected_typeOfService = $('.hidden_user_selection').attr("data-type");

        var hideListing = 0;

        // iterate through each listing
        supplierListing.each(function () {
            // set variables for both 'data-type' and 'data-region-option' for each listing
            var listing_typeOfService = $(this).attr("data-type");
            var listing_regions = $(this).attr('data-region-option').split(",");
            var listing_supplierLocation = $(this).attr('data-supplier-location');
            var listItem = $(this);


            // if nothing is typed in the search box
            if (search_box.val().length === 0) {
                // hide the no results message
                noResults_error.hide();
                // if checkbox_array is empty
                if (checkbox_array < 1) {
                    // if no 'type-of-service' is selected
                    if (selected_typeOfService === undefined) {
                        // iterate through the listing regions
                        for (var i = 0; i < listing_regions.length; i++) {
                            // if the region of the listing is equal to the current selected region
                            if (selected_region === listing_regions[i]) {
                                selected_supplier_location(listing_supplierLocation, listItem)
                            }
                        }
                    }
                    else {
                        // if the 'type of service' selected is equal to the 'data-type' of the listing
                        if (selected_typeOfService === listing_typeOfService) {
                            // iterate through the listing regions
                            for (var i = 0; i < listing_regions.length; i++) {
                                // if the region of the listing is equal to the current selected region
                                if (selected_region === listing_regions[i]) {
                                    selected_supplier_location(listing_supplierLocation, listItem)
                                }
                            }
                        }
                    }

                    // if no region currently selected then
                    if (selected_region === undefined) {
                        // if the selected 'type of service' is equal to the 'data-type' of the listing then
                        if (selected_typeOfService === listing_typeOfService) {
                            selected_supplier_location(listing_supplierLocation, listItem)
                        }

                    }

                    // if no options are selected
                    if (selected_region === undefined && selected_typeOfService === undefined) {
                        selected_supplier_location(listing_supplierLocation, listItem)

                    }

                }
                // if checkbox_array is NOT empty
                else {
                    // set variables
                    var listId = $(this).attr('data-id-options').split(',');
                    var show = false;

                    checkbox_iterate(listId, show, listing_typeOfService, listItem, listing_supplierLocation)
                }

                if ($(this).is(':visible')) {
                    noResults_error.hide();
                }
                else {
                    hideListing++;
                }

            }
            // if the search box is NOT empty
            else {

                // show no results message
                noResults_error.show();
                // if the listing is visible hide the no results message
                if ($(this).is(':visible')) {
                    noResults_error.hide();
                }
                // iterate through the results array
                for (var i = 0; i < fuzzy_search_results.length; i++) {

                    // store each result
                    var $listing = $(".listing.listing-supplier[data-id=" + fuzzy_search_results[i].id + "]");

                    $listing.hide();

                    // for each result
                    $listing.each(function () {

                        // store the 'data-type' and 'data-region-option' attributes
                        var listing_typeOfService = $(this).attr('data-type');
                        var listing_regions = $(this).attr('data-region-option').split(",");
                        var listing_supplierLocation = $(this).attr('data-supplier-location');
                        var listItem = $(this);

                        // if the current selected 'type of service' is equal to 'data-type' of the listing
                        // AND if there is a region currently selected then
                        if (selected_typeOfService === listing_typeOfService && selected_region !== undefined && checkbox_array.length < 1) {

                            // iterate through the regions of the listing
                            for (var i = 0; i < listing_regions.length; i++) {

                                // if the region of the listing is equal to the current selected region
                                if (selected_region === listing_regions[i]) {
                                    selected_supplier_location(listing_supplierLocation, listItem)
                                }
                            }
                        }
                        // if there is no 'region' selected
                        else if (selected_typeOfService === listing_typeOfService && selected_region === undefined && checkbox_array.length < 1) {
                            selected_supplier_location(listing_supplierLocation, listItem)
                        }
                        // if only a region is selected
                        else if (selected_typeOfService === undefined && selected_region !== undefined && checkbox_array.length < 1) {

                            // iterate through the regions of the listing
                            for (var i = 0; i < listing_regions.length; i++) {

                                // if the region of the listing is equal to the current selected region
                                if (selected_region === listing_regions[i] && checkbox_array.length < 1) {
                                    selected_supplier_location(listing_supplierLocation, listItem)

                                }
                            }
                        }
                        // if no selections from either drop downs
                        else if (selected_typeOfService === undefined && selected_region === undefined && checkbox_array.length < 1) {
                            selected_supplier_location(listing_supplierLocation, listItem)
                        }
                        // if checkbox_array contains elements
                        else if (checkbox_array.length > 0) {
                            // set variables
                            var listId = $(this).attr('data-id-options').split(',');
                            var show = false;

                            checkbox_iterate(listId, show, listing_typeOfService, listItem, listing_supplierLocation)
                        }
                        // if listing is visible then hide the no results message
                        if ($(this).is(':visible')) {
                            noResults_error.hide();
                        }

                        // if results is empty then show no results message
                        if (fuzzy_search_results.length < 1) {
                            noResults_error.show();
                        }

                    })
                }
            }

            if (hideListing === supplierListing.length && search_box.val().length === 0) {
                noResults_error.show();
            }
        })
    });


    /*-------------------------
     // Checkbox toggle
     ----------------------------*/
    /**
     * When the user clicks a checkbox, the search results are modified
     */
    $('.filterListBox').find('label').bind('click', function (e) {
        // set count to 0 each time a checkbox is clicked
        count = 0;
        // empty the search box
        search_box.val('');
        // set variables
        var checkBoxId = $(this).parent().children('input').attr('id');
        var index = checkbox_array.indexOf(checkBoxId);

        // if the checkbox is active, removes the active and removes the id from the checkbox_array.
        // Else gives the checkbox the class active and adds the id to the checkbox_array
        if ($(this).parent().hasClass('.active')) {
            $(this).parent().removeClass('.active');
            if (index > -1) {
                checkbox_array.splice(index, 1);
            }
        }
        else {
            $(this).parent().addClass('.active');
            checkbox_array.push(checkBoxId);
        }
        // iterate through each listing
        supplierListing.each(function (i) {
            // set variables
            var listId = $(this).attr('data-id-options').split(',');
            var listing_typeOfService = $(this).attr('data-type');
            var listing_regions = $(this).attr('data-region-option').split(",");
            var selected_region = $('.region-list').find('.inner-text').attr('data-region');
            var listing_supplierLocation = $(this).attr('data-supplier-location');
            var listItem = $(this);
            var show = false;
            // iterate through the checkbox array
            for (var i = 0; i < checkbox_array.length; i++) {
                if (listId.indexOf(checkbox_array[i]) !== -1) { // 'data-id-options' exists in the array
                    show = true; // set to true
                    break; // exit the loop
                }
            }
            // if the checkbox_array is empty and 'data-type' of listing is equal to 'type of service'
            if (checkbox_array.length < 1 && listing_typeOfService === dropDown_typeOfService) {
                if (selected_region !== undefined) { // if selected_region is not undefined
                    for (var i = 0; i < listing_regions.length; i++) { //iterate through the regions of the listings
                        if (selected_region === listing_regions[i]) { // if a region matches the current region
                            selected_supplier_location(listing_supplierLocation, listItem)
                        }
                    }
                }
                else // if region is undefined
                {
                    selected_supplier_location(listing_supplierLocation, listItem)
                }
            }
            else { // if the checkbox_array is NOT empty and/OR 'data-type' of listing is NOT equal to 'type of service'
                if (show && listing_typeOfService === dropDown_typeOfService) { // show is equal to true and 'data-type' of listing is equal to 'type-of-service'
                    if (selected_region !== undefined) { // selected_region is not undefined
                        for (var i = 0; i < listing_regions.length; i++) { //iterate through the regions of the listings
                            if (selected_region === listing_regions[i]) { // if region of listing is equal to the current selected region
                                selected_supplier_location(listing_supplierLocation, listItem)
                            }
                        }
                    }
                    else // selected_region is undefined
                    {
                        selected_supplier_location(listing_supplierLocation, listItem)
                    }
                }
                else if (listing_typeOfService !== dropDown_typeOfService) { //'data-type' of listing is NOT equal to 'type-of-service'
                }
                else {
                    $(this).hide(); // hide listing
                }
            }
            // if listing is visible then do nothing
            if ($(this).is(':visible')) {
            }
            else if ($(this).is(':hidden')) { // if hidden then increment the count
                count++;
            }
            ;
            if (count === supplierListing.length) { // if count is equal to the number of listing (ALL ARE HIDDEN)
                noResults_error.show(); // show error
            }
            else {
                noResults_error.hide(); // hide error
            }
        });
    });

    /*-------------------------
     // Radio Button - ALL
     ----------------------------*/
    /**
     * If the user clicks the ALL radio button the drop down menu will display the menu items for both ukTrade and overseas.
     * @param {evt} The radio button object being clicked.
     */

    rb_all.click(function (evt) {

        console.log('rb all clicked');

        // empty the search box
        search_box.val('');
        noResults_error.hide();
        clear_checkboxes();
        close_listings();

        $(this).addClass('selected');
        rb_overseas.removeClass('selected');
        rb_uk.removeClass('selected');

	    var default_uk_type = uk_typeOfService_dropDown.find('.inner-text-default-uk').text();
	    uk_typeOfService_dropDown.find('.inner-text').text(default_uk_type);
	    uk_typeOfService_dropDown.find('.inner-text').attr('data-type', '');

	    var default_overseas_type = overseas_typeOfService_dropDown.find('.inner-text-default-overseas').text();
	    overseas_typeOfService_dropDown.find('.inner-text').text(default_overseas_type);
	    overseas_typeOfService_dropDown.find('.inner-text').attr('data-type', '');

        overseas_typeOfService_dropDown.hide();
        uk_typeOfService_dropDown.hide();

        $('.filterResults').removeClass('active');

        $('.hidden_user_selection').removeAttr("data-type");

//        supplierListing.show()

        supplierListing.each(function () {

            var selected_region = $('.region-list').find('.inner-text').attr('data-region');
            var listing_typeOfService = $(this).attr('data-type');

            dataAttrValidation(selected_region, selected_typeOfService);

            if (selected_region === undefined && dropDown_typeOfService !== listing_typeOfService) {
                $(this).show();
            }

        });

    });

    /*-------------------------
     // Radio Button - UK
     ----------------------------*/
    /**
     * If the user clicks the ukTrade button the drop down menu will display the menu items for ukTrade.
     * On click displays the hidden drop down menu and hides the overseas menu.
     *
     * @param {evt} The radio button object being clicked.
     */

    rb_uk.click(function (evt) {

        // empty the search box
        search_box.val('');
        noResults_error.hide();
        clear_checkboxes();
        close_listings();

        $(this).addClass('selected');
        rb_overseas.removeClass('selected');
        rb_all.removeClass('selected');

        overseas_typeOfService_dropDown.hide();
        uk_typeOfService_dropDown.show();

        var default_uk_type = uk_typeOfService_dropDown.find('.inner-text-default-uk').text();
        uk_typeOfService_dropDown.find('.inner-text').text(default_uk_type);
        uk_typeOfService_dropDown.find('.inner-text').attr('data-type', '');

        $('.filterResults').removeClass('active');

        $('.hidden_user_selection').removeAttr("data-type");

        var selected_typeOfService = uk_typeOfService_dropDown.find('.inner-text').attr('data-type');

        supplierListing.each(function () {

            var selected_region = $('.region-list').find('.inner-text').attr('data-region');

            if ($(this).attr('data-supplier-location') === "uk") {

                dataAttrValidation(selected_region, selected_typeOfService)

                if (selected_region === undefined && selected_typeOfService === '') {
                    $(this).show();
                }
            }
            else {
                $(this).hide();
            }

        });

    });

    /*-------------------------
     // Radio Button - Overseas
     ----------------------------*/
    /**
     * If the user clicks the overseas button the drop down menu will display the menu items for overseas.
     * On click displays the hidden drop down menu and hides the uk menu.
     *
     * @param {evt} The radio button object being clicked.
     */

    rb_overseas.click(function (evt) {

        // empty the search box
        search_box.val('');
        noResults_error.hide();
        clear_checkboxes();
        close_listings();

        $(this).addClass('selected');
        rb_uk.removeClass('selected');
        rb_all.removeClass('selected');

        overseas_typeOfService_dropDown.show();
        uk_typeOfService_dropDown.hide();

		var default_overseas_type = overseas_typeOfService_dropDown.find('.inner-text-default-overseas').text();
        overseas_typeOfService_dropDown.find('.inner-text').text(default_overseas_type);
        overseas_typeOfService_dropDown.find('.inner-text').attr('data-type', '');

        $('.hidden_user_selection').removeAttr("data-type");

        var selected_typeOfService = overseas_typeOfService_dropDown.find('.inner-text').attr('data-type');

        $('.filterResults').removeClass('active');

        supplierListing.each(function () {

            var selected_region = $('.region-list').find('.inner-text').attr('data-region');

            if ($(this).attr('data-supplier-location') === "overseas") {

                dataAttrValidation(selected_region, selected_typeOfService)

                if (selected_region === undefined && selected_typeOfService === '') {
                    $(this).show();
                }
            }
            else {
                $(this).hide();
            }

        });
    });


    /*-----------------------------------------------
     // Toggle Supplier Listing (Open / Close)
     -----------------------------------------------*/
    /**
     * Allows for the user to toggle the supplier listings.
     */
    supplierListing.bind('click', function (e) {
        if ($(this).hasClass('open') && ($(e.target).hasClass('supplier-view') || $(e.target).hasClass('plus'))) {
            $(this).removeClass('open');
        } else if (!$(this).hasClass('open')) {

            $(this).addClass('open');
        }
    });

    /**
     * Allows for the user to toggle the itineraries.
     */
    $('.itinerary').bind('click', function (e) {
        if ($(this).hasClass('open') && ($(e.target).hasClass('view') || $(e.target).hasClass('plus'))) {
            $(this).removeClass('open');
        } else if (!$(this).hasClass('open')) {

            $(this).addClass('open');
        }
    });

});