var map = null;
var p = document.createElement('p').style;
var supportsTransitions = 'transition' in p ||
                          'WebkitTransition' in p ||
                          'MozTransition' in p ||
                          'msTransition' in p ||
                          'OTransition' in p;

var supportsTransform = 'transform' in p ||
                        'WebkitTransform' in p ||
                        'MozTransform' in p ||
                        'msTransform' in p ||
                        'OTransform' in p;

function getPrefixed(prop) {
  var i, s = document.createElement('p').style, v = ['ms','O','Moz','Webkit'];
  if(s[prop] == '')  {
    return prop;
  }
  prop = prop.charAt(0).toUpperCase() + prop.slice(1);
  for(i = v.length; i--;) {
    if(s[v[i] + prop] == '' ) {
      return (v[i] + prop);
    }
  }
}

var sliders = { };

$(function(){

  var search_box = $('.searchBox');
  var itinListing = $('.listing');
  var noResults_error = $('.noResults');

  noResults_error.hide();


  $('.itinerary').bind('click', function(e) {
    if($(this).hasClass('open') && ($(e.target).hasClass('view') || $(e.target).hasClass('plus'))) {
      $(this).removeClass('open');
    } else if(!$(this).hasClass('open')) {
      if(sliders[$(this).attr('data-id')] == undefined) {
        sliders[$(this).attr('data-id')] = new Slider({ element: $(this) });
      }
      $(this).addClass('open');
    }
  });
  $('[data-map]').on('click', function(e) {
    e.preventDefault;
    if(map == null) {
      map = new MapTool({ wrapper: $('.mapTool')[0], element: $('.mapTool > .map')[0] });
    }
    map.load($(this).attr('data-map'), $('.itinerary').attr('data-lang'));
  });


  /*--------------------------------------
      Toggle the Listings
  --------------------------------------*/

    $('.listing-itin').bind('click', function (e) {
        if ($(this).hasClass('open') && ($(e.target).hasClass('supplier-view') || $(e.target).hasClass('plus'))) {
            $(this).removeClass('open');
        } else if (!$(this).hasClass('open')) {
            $(this).addClass('open');
        }
    });  

  /*------------------------------------------
   // Beginning of Fuzzy Search Section
   ---------------------------------------------*/

  var searchArr = [];

  itinListing.each(function (i) {

    var $infoHeader = $(this).find('.infoHeader').text();
    var $id = $(this).attr("data-id");
    var $lang = $(this).attr("data-lang");
    var temp = {
        id: $id,
        lang: $lang,
        title: $infoHeader
    };
    searchArr.push(temp);
  });

  var options = {
      keys: ['title']
  }

  noResults_error.hide();

  var showVisibleItins = function(results) {

        for(var i = 0; i < results.length; i++) { 

          itinListing.each( function() {
            var $dataId = $(this).data('id');
            if($dataId === results[i].id) {
              $(this).show();
            }
          });          
        }

  }

  search_box.on('change paste keyup', function () {

        // fuzzy search
        var f = new Fuse(searchArr, options);
        fuzzy_search_results = f.search(search_box.val());

        // hide all listings
        itinListing.hide();

        showVisibleItins(fuzzy_search_results);

        if(fuzzy_search_results.length < 1) {
          if(search_box.val().length < 1) {
            itinListing.show();
            noResults_error.hide();
          } else {
            noResults_error.show();
          }          
        } else if(fuzzy_search_results.length >= 1) {

          noResults_error.hide();
        }
    });
});