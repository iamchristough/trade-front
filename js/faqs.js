$(function () {
    // initialise count
    var count;

    $('.faq').on('click', function () {
        $(this).toggleClass('open');
    });

    $("h2.drop_down-faq").click(function (evt) {

        var dropdownFaq = $(this).parents('.faqDropDown').find('.drop_downUL');

        if (dropdownFaq.hasClass('active')) {
            $(".drop_downUL").removeClass("active");
        }
        else {
            $(".drop_downUL").removeClass("active");
            evt.stopPropagation();
            var $ul = $(this).siblings('.drop_downUL');
            $ul.addClass("active");

        }
    });

    var faqArr = [];


    /*-------------------------
     // Fuzzy Search
     ----------------------------*/
    $('.faq').each(function (i) {
        // set vars
        var $pAnswerTags = $(this).children('.answerSection').children('.faq-answer').children('p');
        var $question = $(this).children('.q_header').children('.faq-question').find('.question').text();
        var $category = $(this).children('.q_header').children('.faq-question').find('.category').text();
        var $answer = $(this).children('.answerSection').children('.faq-answer').find('.answer')
            .text();
        var $id = $(this).attr("data-id");

        $pAnswerTags.each(function (i) {
            $(this).addClass('answer');
        });
        // create fuzzy search
        var temp = {
            id: $id,
            question: $question,
            category: $category,
            answer: $answer
        };
        faqArr.push(temp);
    });

    var options = {
        keys: ['question']
    }

    var selectedCategory = 0;

    /*-------------------------
     // Search Input - Keyboard Press
     ----------------------------*/

    $('.searchBox').on('change paste keyup', function () {
        $('.noResultsFaq').hide();

        var f = new Fuse(faqArr, options);
        var $result = f.search($('.searchBox').val());
        var $faqs = $(this).parents('.content.noShadow').find('.faq');

        if ($result.length < 1 && $('.searchBox').val().length !== 0) {
            $('.noResultsFaq').show();
        }
        // hide all faqs
        $(".faq").hide();

        // if nothing typed
        if ($('.searchBox').val().length === 0) {
            // if 'all topics' or no selection, show all faqs
            if (selectedCategory <= 1) {
                $(".faq").show();
            } else {
                // iterate through faqs
                $faqs.each(function () {
                    // faq id equal to the selected category
                    if ($(this).attr("data-category-id") === selectedCategory) {
                        $(this).show();
                    }
                    else {
                        $(this).hide();
                    }
                })
            }
        }
        // iterate through fuzzy search results
        for (var i = 0; i < $result.length; i++) {
            var $faq = $(".faq[data-id=" + $result[i].id + "]");

            if ($faq.attr("data-category-id") === selectedCategory) {
                $faq.show();
            }
            else if (selectedCategory <= 1) {
                $faq.show();
            }
        }
        count = 0;

        // creates count for error message
        $faqs.each(function () {
            if ($(this).is(':visible')) {
            }
            else if ($(this).is(':hidden')) {
                count++
            }

        })
        // if all faqs are hidden then show error message
        if (count === $faqs.length) {
            $('.noResultsFaq').show();
        }

    });


    /*-------------------------
     // Drop Down - List Item Click
     ----------------------------*/
    $('.drop_downUL').on('click', 'li', function () {
        $('.noResultsFaq').hide();

        $('.inner-text').removeClass('default');
        $('drop_downUL').removeClass('active');

        $(this).addClass('active');

        var $faqs = $(this).parents('.content.noShadow').find('.faqBody').children('.faq');
        var faqCatId = $(this).attr("data-category-id");
        var selectedValue = $(this).text();

        $('.selectedTopicInput').val(selectedValue);

        var newItem = $('.selectedTopicInput').val();

        $(this).parents('li').children('.drop_down').find('.inner-text').text(newItem);
        $(".drop_downUL").removeClass("active");

        selectedCategory = faqCatId;
        $('.searchBox').val("");

        $faqs.each(function () {

            if ($(this).attr("data-category-id") === faqCatId) {

                $(this).show();
            }
            else if (faqCatId == 1) {
                $(this).show();
            }
            else {
                $(this).hide();
            }

            if($(this).hasClass('open')) {
                $(this).toggleClass('open');
            }
        })
    });

    $('html').click(function () {
        $(".drop_downUL").removeClass('active');
    });

});
