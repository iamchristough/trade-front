$( document ).ready(function() {

	$('.explore-button').on('click', function(ev) {
		ev.preventDefault();
		$('.wrapper').toggleClass('hide');
		$('.explore-icon').toggleClass('active');
		$(this).toggleClass('active');
		if ($(this).hasClass('active')) {
		    $('.discover').addClass('open');
		    $('.show-background').addClass('hide');
		    $('.hide-background').removeClass('hide');
		} else {
		    $('.discover').removeClass('open');
		    $('.show-background').removeClass('hide');
		    $('.hide-background').addClass('hide');
		}
	});

});